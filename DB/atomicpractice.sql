-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2016 at 12:37 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicpractice`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`) VALUES
(1, 'C Code'),
(2, 'Teach Yourself C'),
(3, 'Me Before You'),
(4, 'Paper Town'),
(5, ''),
(6, 'The Book Thief'),
(7, 'hello there12'),
(10, 'aym'),
(11, 'hello hi'),
(12, ''),
(13, 'Helo');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `city` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `email`, `deleted_at`) VALUES
(1, 'Aym', 'London', 'ayman_fia@ymail.com', NULL),
(2, 'Ayman', 'Sydney', 'ayman.fia@gmail.com', NULL),
(3, 'Sarah Sohana', 'London', 'sohana.27@gmail.com', NULL),
(4, 'Fayeq', 'London', 'fayek123@gmail.com', NULL),
(6, 'Jolil', 'Sydney', 'xyz.123@yahoo.com', NULL),
(7, 'Afiya Ayman', 'Dhaka', 'ayman_fia@ymail.com', NULL),
(8, 'Sarah Sohana', 'Dhaka', 'sohana.27@gmail.com', NULL),
(9, 'Fayeq', 'Sydney', 'fayek123@gmail.com', NULL),
(10, 'Aym', 'Ohio', 'ayman_fia@ymail.com', NULL),
(11, 'Jo', 'Dhaka', 'abc_17@yahoo.com', NULL),
(12, 'Sarah Sohana', 'Sydney', 'sohana.27@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `hobbies` varchar(300) NOT NULL,
  `deleted_at` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobbies`, `deleted_at`) VALUES
(1, 'Aym', 'Photography,Coding', NULL),
(2, 'Fa', 'Photography,Fishing,Gaming', NULL),
(5, 'Adnan', 'Teaching,Coding,Fishing,Travelling,Gaming,Cricket,Football', '1466884808'),
(6, 'Hi', 'Teaching,Coding,Gardening,Gaming', NULL),
(7, 'Ayman', 'Coding', NULL),
(8, 'as', 'Coding', NULL),
(9, 'aym', 'Teaching', NULL),
(10, 'Afi', 'Photography,Teaching', NULL),
(11, 'saru', 'Fishing', '1466884410'),
(12, 'Hello', 'Photography,Teaching,Gardening', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `images` varchar(300) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(1, 'Aym', '', NULL),
(2, 'Aym', '', NULL),
(3, 'Aym', '', NULL),
(4, 'Aym', '14681308316bc537a241ffc746acb7d2180d2253d8.jpg', NULL),
(5, 'Jo', '14681318372015-11-27-22-07-11-673-2.jpg', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
