<?php
namespace App\Bitm\SEIP139740\Book;

use App\Bitm\SEIP139740\Message\Message;
use App\Bitm\SEIP139740\Utility\Utility;

class Book
{
    public $id;
    public $title;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicpractice") or die("Database Connection Failed");
    }


    public function prepare($data="")
    {
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if (array_key_exists('title',$data))
        {
            $this->title=$data['title'];
        }

    }

    public function store()
    {
        $query="INSERT INTO `atomicpractice`.`book`(`title`) VALUE ('".$this->title."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Data has been inserted");
            //header("Location:","index.php");
            Utility::redirect("index.php");
        }
        else
        {
            echo "Error";
        }
    }

    public function index()
    {
        $_allBook=array();
        $query="SELECT * FROM `book`";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result))
        {
            $_allBook[]=$row;
        }
        return $_allBook;
    }

    public function count()
    {
        $query="SELECT COUNT(*) AS totalitem FROM `book`";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row->totalitem;
    }
    public function paginate($pageStartFrom=0,$limit=5)
    {
        $_allBook=array();
        $query="SELECT * FROM `book` LIMIT ".$pageStartFrom.",".$limit;
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result))
        {
            $_allBook[]=$row;
        }
        return $_allBook;
    }

    public function view()
    {
        $query="SELECT * FROM `book` WHERE `id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query="UPDATE `book` SET `title` = '".$this->title."'WHERE `book`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Data has been updated");
            Utility::redirect("index.php");
        }
        else
        {
            echo "Error";
        }
    }

    public function delete()
    {
        $query="DELETE FROM `atomicpractice`.`book` WHERE `book`.`id`=".$this->id;
        echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Data has been deleted");
            Utility::redirect('index.php');
        }
        else
        {
            echo "Error";
        }

    }



}