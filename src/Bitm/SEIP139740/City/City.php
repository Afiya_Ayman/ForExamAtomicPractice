<?php
namespace App\Bitm\SEIP139740\City;
use App\Bitm\SEIP139740\Utility\Utility;
use App\Bitm\SEIP139740\Message\Message;
class City
{
    public $id;
    public $city;
    public $name;
    public $email;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicpractice") or die("DB Failed");
    }
    public function prepare($data="")
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(array_key_exists('city',$data))
        {
            $this->city=$data['city'];
        }
        if(array_key_exists('email',$data))
        {
            $this->email=$data['email'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `city`(`name`,`city`,`email`) VALUES ('".$this->name."','".$this->city."','".$this->email."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
                <div class=\"alert alert - success\">
                <strong>Success</strong>Data Has been stored.
                </div>"
            );
            Utility::redirect('index.php');
        }else{
            Message::message("
            <div class=\"alert alert - success\">
                <strong>Error!</strong>Data Has not been stored.
                </div>"
            );
        }
    }

    public function index()
    {
        $_allUser=array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function count()
    {
        $query="SELECT COUNT(*) AS totalitem FROM `city`";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row['totalitem'];
    }

    public function paginate($pageStartFrom=0,$limit=5)
    {
        $_allUser=array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$limit;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function view()
    {
        $query="SELECT * FROM `city` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
    public function update()
    {
        $query="UPDATE `city` SET `name`='".$this->name."',`city`='".$this->city."',`email`='".$this->email."' WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
                <div class=\"alert alert - success\">
                <strong>Success</strong>Data Has been Updated.
                </div>"
            );
            Utility::redirect('index.php');
        }else{
            Message::message("
            <div class=\"alert alert - success\">
                <strong>Error!</strong>Data Has not been Updated.
                </div>"
            );
        }
    }

    public function delete()
    {
        $query="DELETE FROM `city` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
                <div class=\"alert alert - success\">
                <strong>Success</strong>Data Has been deleted.
                </div>"
            );
            Utility::redirect('index.php');
        }else{
            Message::message("
            <div class=\"alert alert - success\">
                <strong>Error!</strong>
                </div>"
            );
        }
    }
}