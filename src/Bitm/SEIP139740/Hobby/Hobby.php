<?php

namespace App\Bitm\SEIP139740\Hobby;
use App\Bitm\SEIP139740\Message\Message;
use App\Bitm\SEIP139740\Utility\Utility;

class Hobby
{
    public $id;
    public $name;
    public $hobby;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicpractice") or die("Database Connection Failed");
    }

    public function prepare($data="")
    {
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if (array_key_exists('hobby',$data))
        {
            $this->hobby=$data['hobby'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `atomicpractice`.`hobby`(`name`,`hobbies`) VALUES ('".$this->name."','".$this->hobby."')";
        echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("<div class=\"alert alert - info\">
            <strong>Stored!</strong> Your Hobby has been Stored.
            </div>");
            Utility::redirect('index.php');
        }else{
            echo "No!";
        }
    }

    public function index()
    {
        $_allUser=array();
        $query="SELECT * FROM `atomicpractice`.`hobby` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function view()
    {
        $query="SELECT * FROM `atomicpractice`.`hobby` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query="UPDATE `atomicpractice`.`hobby` SET `name`='".$this->name."',`hobbies`='".$this->hobby."' WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes!";
        }else{
            echo "No!";
        }
    }

    public function delete()
    {
        $query="DELETE FROM `atomicpractice`.`hobby` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes!";
        }else{
            echo "No!";
        }
    }

    public function trash()
    {
        $query="UPDATE `atomicpractice`.`hobby` SET `deleted_at`='".time()."' WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes!";
        }else{
            echo "No!";
        }
    }

    public function trashedView()
    {
        $_allUser=array();
        $query="SELECT * FROM `atomicpractice`.`hobby` WHERE `deleted_at` IS NOT NULL ";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function recover()
    {
        $query="UPDATE `atomicpractice`.`hobby` SET `deleted_at`=NULL WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes!";
        }else{
            echo "No!";
        }
    }

    public function recoverMultiple($idS=array())
    {
        if(is_array($idS) && count($idS)>0)
        {
            $IDs=implode(",",$idS);
            $query="UPDATE `atomicpractice`.`hobby` SET `deleted_at`=NULL WHERE `id`IN (".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                echo "Yes!";
            }else{
                echo "No!";
            }
        }
    }

    public function deleteMultiple($idS=array())
    {
        if(is_array($idS) && count($idS)>0)
        {
            $IDs=implode(",",$idS);
            $query="DELETE FROM `atomicpractice`.`hobby` WHERE `id`IN (".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                echo "Yes!";
            }else{
                echo "No!";
            }
        }
    }





}