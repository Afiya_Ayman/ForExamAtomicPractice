<?php
namespace App\Bitm\SEIP139740\Message;
if(!(isset($_SESSION['message'])))
{
    session_start();
}

class Message
{

    public static function message($message=NULL)
    {
        if(is_null($message))
        {
            $_message=self::getMessage();
            return $_message;

        }
        else
        {
            self::showMessage($message);
        }
    }

    public static function showMessage($message)
    {
        $_SESSION['message']=$message;
    }

    public static function getMessage()
    {
        $_message=$_SESSION['message'];
        $_SESSION['message']="";
        return $_message;

    }
}