<?php
namespace App\Bitm\SEIP139740\ProfilePicture;
class ImageUploader
{
    public $id;
    public $name;
    public $image;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicpractice") or die("DB failed");
    }

    public function prepare($data)
    {
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if (array_key_exists('images',$data))
        {
            $this->image=$data['images'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `atomicpractice`.`profilepicture`(`name`,`images`) VALUES ('".$this->name."','".$this->image."')";
        echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Success!";
        }else{
            echo "error!";
        }
    }

    public function index()
    {
        $_allUser=array();
        $query="SELECT * FROM `atomicpractice`.`profilepicture`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function view()
    {
        $query="SELECT * FROM `atomicpractice`.`profilepicture` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
}
