<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Book\Book;
use App\Bitm\SEIP139740\Utility\Utility;

$book=new Book();
$book->prepare($_GET);
$book->delete();