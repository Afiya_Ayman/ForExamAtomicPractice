<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Book\Book;
use App\Bitm\SEIP139740\Utility\Utility;

$book=new Book();
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">

</head>

<body>


<div class="container">
    <center><h2>Book Title</h2></center>
    <br>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Enter Book Title</label>
            <input type="hidden" name="id" value="<?php echo $singleItem->id; ?>">
            <input type="text" class="form-control" name="title" id="book" placeholder="Enter book title" value="<?php echo $singleItem->title; ?>">
        </div>
        <button type="submit" class="btn-group">Submit</button>
    </form>
</div>



</body>
</html>
