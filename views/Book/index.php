<?php
session_start();
include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Book\Book;
use App\Bitm\SEIP139740\Message\Message;
use App\Bitm\SEIP139740\Utility\Utility;

$book=new Book();
$book->prepare($_GET);
//$allBook=$book->index();

    if(array_key_exists('itemPerPage',$_SESSION))
    {
        if(array_key_exists('itemPerPage',$_GET))
        {
            $_SESSION['itemPerPage']=$_GET['itemPerPage'];
        }
    }else{
        $_SESSION['itemPerPage']=5;
    }

    $itemPerPage=$_SESSION['itemPerPage'];

    $totalitem=$book->count();
    $totalPage=ceil($totalitem/$itemPerPage);

    if(array_key_exists('pageNumber',$_GET))
    {
        $pageNumber=$_GET['pageNumber'];
    }
    else
    {
        $pageNumber=1;
    }
    //Utility::d($totalPage);
    $pagination="";

    for($i=1;$i<=$totalPage;$i++)
    {
        $class=($pageNumber==$i)?"active":"";
        $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
    }
    $StartPage=$itemPerPage*($pageNumber-1);
    $prevPage=$pageNumber-1;
    $nextPage=$pageNumber+1;
    $allBook=$book->paginate($StartPage,$itemPerPage);




?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Book List</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
</head>

<body>

<div class="container">

    <center><h2>All Book List</h2></center>

        <br><br>
        <a href="create.php" class="btn btn-primary" role="button">Insert Again</a>
        <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
        <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
        <a href="mail.php" class="btn btn-primary" role="button">Mail to A Friend</a>
        <div id="message">
        <?php
        echo Message::message();
        ?>
            </div>
    <br>
    <form role="form">
        <div class="form-group">
            <select class="form-control" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                </select>
            <button type="submit" class="btn btn-primary">GO</button>
            </div>
        </form>
        <br><br>
    <div class="table-responsive">
        <table class="table">
        <thead>
        <tr>
            <th>Sl</th>
            <th>ID</th>
            <th>Book Title</th>
            <th>Action</th>

        </tr>
        </thead>

        <tbody>
        <?php $sl=1; ?>
        <tr>
            <?php
            foreach($allBook as $book)
            {
            ?>
            <td><?php echo $sl++; ?></td>
            <td><?php echo $book->id; ?></td>
            <td><?php echo $book->title; ?></td>
            <td>
                <a href="view.php?id=<?php echo $book->id; ?> " class="btn btn-info" role="button">View</a>
                <a href="edit.php?id=<?php echo $book->id;?>" class="btn btn-primary" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $book->id?>" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
        <?php } ?>
        </tbody>
            </table>

    </div>
    </div>

<center><ul class="pagination">
        <?php if($pageNumber>1){ ?>
    <li><a href="index.php?pageNumber=<?php echo $prevPage?>">Prev</a></li>
        <?php } ?>
        <li><?php echo $pagination?></li>
        <?php if($pageNumber<$totalPage){ ?>
            <li><a href="index.php?pageNumber=<?php echo $nextPage?>">Next</a></li>
        <?php } ?>
    </ul></center>


<script>
    $('#message').show().delay(2000).fadeout()
</script>



</body>




    </html>
