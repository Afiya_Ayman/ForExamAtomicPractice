<?php
require_once('../../vendor/mpdf/mpdf/mpdf.php');
include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Book\Book;

$book=new Book();
$allBook=$book->index();

$trs="";
$sl=0;
foreach($allBook as $book):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>$sl</td>";
    $trs.="<td>$book->id</td>";
    $trs.="<td>$book->title</td>";
    $trs.="</tr>";
    endforeach;

$html=<<<EOD
<table class="table">
        <thead>
        <tr>
            <th>Sl</th>
            <th>ID</th>
            <th>Book Title</th>

        </tr>
        </thead>

        <tbody>
           $trs
        </tbody>
            </table>
EOD;

$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();