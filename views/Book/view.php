<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Book\Book;
use App\Bitm\SEIP139740\Utility\Utility;

$book=new Book();
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>View Book Title</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">

</head>

<body>


<div class="container">
    <center><h2>View Book Title</h2></center>
    <br>
    <ul class="list-group">

        <li class="list list-group"><strong>ID: </strong><?php echo " ".$singleItem->id ?></li>
        <li class="list list-group"><strong>Title: </strong><?php echo " ".$singleItem->title ?></li>

    </ul>
</div>



</body>
</html>
