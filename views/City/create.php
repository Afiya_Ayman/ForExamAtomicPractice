<!Doctype html>
    <html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h2>Insert City</h2>
    <br>
    <form role="form" action="store.php" method="post" class="form-group">

        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Your Name">
            </div>
        <div class="form-group">
            <label>City</label>
            <select class="form-control" name="city">
                <option value="New York">New York</option>
                <option value="London">London</option>
                <option value="Sydney">Sydney</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Paris">Paris</option>
                <option value="Ohio">Ohio</option>
                <option value="Manhatton">Manhatton</option>
            </select>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" placeholder="Enter Your Email">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>