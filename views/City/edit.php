<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\City\City;
use App\Bitm\SEIP139740\Utility\Utility;

$obj=new City();
$obj->prepare($_GET);
$singleItem=$obj->view();
//Utility::d($singleItem);

?>

<!Doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h2>Edit City</h2>
    <br>
    <form role="form" action="update.php" method="post" class="form-group">

        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singleItem->id?>">
            <label>Name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $singleItem->name?>">
        </div>
        <div class="form-group">
            <label>City:(Select One)</label>
            <select class="form-control" name="city">
                <option <?php if($singleItem->city=="New York"){?> selected="selected"<?php } ?>>New York</option>
                <option <?php if($singleItem->city=="London"){?> selected="selected"<?php } ?>>London</option>
                <option <?php if($singleItem->city=="Sydney"){?> selected="selected"<?php } ?>>Sydney</option>
                <option <?php if($singleItem->city=="Dhaka"){?> selected="selected"<?php } ?>>Dhaka</option>
                <option <?php if($singleItem->city=="Paris"){?> selected="selected"<?php } ?>>Paris</option>
                <option <?php if($singleItem->city=="Ohio"){?> selected="selected"<?php } ?>>Ohio</option>
                <option <?php if($singleItem->city=="Manhatton"){?> selected="selected"<?php } ?>>Manhatton</option>
            </select>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $singleItem->email?>">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
</body>
</html>