<?php
session_start();
include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\City\City;
use App\Bitm\SEIP139740\Message\Message;
use App\Bitm\SEIP139740\Utility\Utility;

$user=new City();
$allUser=$user->index();

    if(array_key_exists('itemPerPage',$_SESSION))
    {
        if(array_key_exists('itemPerPage',$_GET))
        {
            $_SESSION['itemPerPage']=$_GET['itemPerPage'];
        }
    }else
    {
        $_SESSION['itemPerPage']=5;
    }
    $itemPerPage=$_SESSION['itemPerPage'];
    $totalitem=$user->count();
    $totalPage=ceil($totalitem/$itemPerPage);
    //Utility::d($_GET);

    $pagination="";

    if(array_key_exists('pageNumber',$_GET))
    {
        $pageNumber=$_GET['pageNumber'];
    }else
    {
        $pageNumber=1;
    }

    for($i=1;$i<=$totalPage;$i++)
    {
        $class=($pageNumber==$i)?"active":"";
        $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
    }

    $pageStartFrom=$itemPerPage*($pageNumber-1);
    $prevPage=$pageNumber-1;
    $nextPage=$pageNumber+1;

    $allUser=$user->paginate($pageStartFrom,$itemPerPage);






?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>List of Cities of Users</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">

    <center><h2>List of Cities of Users</h2></center>
        <br><br>
        <a href="create.php" class="btn btn-primary" role="button">Insert Again</a>
        <br><br>
    <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
        <form role="form">
            <div class="form-group">
            <label>Select Item Per Page to display</label>
            <select class="form-control" name="itemPerPage">
                <option <?php if($itemPerPage==5){?> selected="selected" <?php } ?>>5</option>
                <option <?php if($itemPerPage==10){?> selected="selected" <?php } ?>>10</option>
                <option <?php if($itemPerPage==15){?> selected="selected" <?php } ?>>15</option>
                <option <?php if($itemPerPage==20){?> selected="selected" <?php } ?>>20</option>
            </select>
            <button type="submit" class="btn btn-primary">GO</button>
                </div>
        </form>
        <div id="message">
            <?php echo Message::message();?>
            </div>
        <br><br>
    <div class="table-responsive">
        <table class="table">
        <thead>
        <tr>
            <th>Sl</th>
            <th>ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Email</th>
            <th>Action</th>

        </tr>
        </thead>

        <tbody>
        <?php $sl=1; ?>
        <tr>
            <?php
            foreach($allUser as $data)
            {
            ?>
            <td><?php echo $sl++; ?></td>
            <td><?php echo $data->id; ?></td>
            <td><?php echo $data->name; ?></td>
            <td><?php echo $data->city; ?></td>
            <td><?php echo $data->email; ?></td>
            <td>
                <a href="view.php?id=<?php echo $data->id; ?> " class="btn btn-info" role="button">View</a>
                <a href="edit.php?id=<?php echo $data->id;?>" class="btn btn-primary" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $data->id?>" class="btn btn-danger" role="button">Delete</a>
                <a href="email.php?id=<?php echo $data->id?>" class="btn btn-warning" role="button">Mail User</a>
            </td>
        </tr>
        <?php } ?>
        </tbody>
            </table>

    </div>
    <center><ul class="pagination">
            <?php if($pageNumber>1){?>
            <li><a href="index.php?pageNumber=<?php echo $prevPage?>">Prev</a></li>
            <?php } ?>
        <li><?php echo $pagination?></li>
            <?php if($pageNumber<$totalPage){?>
                <li><a href="index.php?pageNumber=<?php echo $nextPage?>">Next</a></li>
            <?php } ?>
        </ul></center>
    </div>


<script>
   $('#message').show().delay(2000).fadeOut();
</script>



</body>




    </html>
