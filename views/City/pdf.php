<?php

// Require composer autoload
require_once('../../vendor/mpdf/mpdf/mpdf.php');
include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\City\City;

$obj=new City();
$allUser=$obj->index();

$trs="";
$sl=0;
foreach($allUser as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>$sl;</td>";
    $trs.="<td>$data->id</td>";
    $trs.="<td>$data->name</td>";
    $trs.="<td>$data->city</td>";
    $trs.="<td>$data->email</td>";
    $trs.="</tr>";
    endforeach;

$html=<<<EOD
<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>

<div class="container">

    <center><h2>List of Cities of Users</h2></center>
        <br>
    <div class="table-responsive">
        <table class="table">
        <thead>
        <tr>
            <th>Sl</th>
            <th>ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Email</th>

        </tr>
        </thead>
            $trs
        <tbody>
        </tbody>
            </table>

    </div>
</body>
    </html>

EOD;



$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf','D');