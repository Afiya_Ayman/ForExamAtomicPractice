<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\City\City;
use App\Bitm\SEIP139740\Utility\Utility;

$obj=new City();
$obj->prepare($_GET);
$singleItem=$obj->view();
//Utility::d($singleItem);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>View User</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">

</head>

<body>


<div class="container">
    <center><h2>View User</h2></center>
    <br>
    <ul class="list-group">
        <li class="list-group-item list-group-item-success">ID:<strong><?php echo $singleItem->id?></strong></li>
        <li class="list-group-item list-group-item-success">Name:<strong><?php echo $singleItem->name?></strong></li>
        <li class="list-group-item list-group-item-success">City:<strong><?php echo $singleItem->city?></strong></li>
        <li class="list-group-item list-group-item-success">Email:<strong><?php echo $singleItem->email?></strong></li>
    </ul>
</div>



</body>
</html>
