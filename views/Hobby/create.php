<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
    <head>
    <title>Hobby</title>
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="../../Resource/js/bootstrap.min.js"></script>

    </head>


<body>
    <div class="container">

        <center><h2>Enter Your Name & Select Your Hobbies From the List Below</h2></center>

        <form class="form-group" role="form" method="post" action="store.php">
            <div class="container">
                <label>Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter name: ">
            </div>
            <br>
            <div class="container">
            <h3>Choose Hobby</h3>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Photography">Photography</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Teaching">Teaching</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Fishing">Fishing</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Travelling">Travelling</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Gaming">Gaming</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Cricket">Cricket</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Football">Football</label></div>

            <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        </div>
</body>

</html>