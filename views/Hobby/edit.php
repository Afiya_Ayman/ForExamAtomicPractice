<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Hobby\Hobby;
use App\Bitm\SEIP139740\Utility\Utility;

$user=new Hobby();
$user->prepare($_GET);
$singleItem=$user->view();
$stringToArray=explode(",",$singleItem->hobbies);
Utility::d($stringToArray);
?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<head>
    <title>Hobby</title>
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="../../Resource/js/bootstrap.min.js"></script>

</head>


<body>
<div class="container">

    <center><h2>Update Your Name & Hobbies</h2></center>

    <form class="form-group" role="form" method="post" action="update.php">
        <div class="container">
            <label>Name</label>
            <input type="hidden" name="id" value="<?php echo $singleItem->id?>">
            <input type="text" class="form-control" name="name" value="<?php echo $singleItem->name?>">
        </div>
        <br>
        <div class="container">
            <h3>Update Hobby</h3>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Photography"
                        <?php if(in_array('Photography',$stringToArray))
                        {echo "checked";}?> >Photography</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Teaching"
                        <?php if(in_array('Teaching',$stringToArray))
                        {echo "checked";}?>>Teaching</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Coding"
                        <?php if(in_array('Coding',$stringToArray))
                        {echo "checked";}?>>Coding</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Fishing"
                        <?php if(in_array('Fishing',$stringToArray))
                        {echo "checked";}?>>Fishing</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Gardening"
                        <?php if(in_array('Gardening',$stringToArray))
                        {echo "checked";}?>>Gardening</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Travelling"
                        <?php if(in_array('Travelling',$stringToArray))
                        {echo "checked";}?>>Travelling</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Gaming"
                        <?php if(in_array('Gaming',$stringToArray))
                        {echo "checked";}?>>Gaming</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Cricket"
                        <?php if(in_array('Cricket',$stringToArray))
                        {echo "checked";}?>>Cricket</label></div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Football"
                        <?php if(in_array('Football',$stringToArray))
                        {echo "checked";}?>>Football</label></div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
</div>
</body>

</html>
