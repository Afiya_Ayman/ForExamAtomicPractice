<?php
session_start();
include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Hobby\Hobby;
use App\Bitm\SEIP139740\Utility\Utility;
use App\Bitm\SEIP139740\Message\Message;

$user=new Hobby();
$allUser=$user->index();


?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<head>
    <title>Hobby</title>
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="../../Resource/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>


</head>


<body>
<div class="container">
    <center><h2>All Hobbies of Users</h2></center>
    <br><br>

    <a href="create.php" class="btn btn-primary">Insert Again</a>
    <div id="message">
    <?php echo Message::message();?>
        </div>
    <br><br>
    <a href="trashedView.php" class="btn btn-info">Trashed Hobbies</a>
    <br><br>
    <table class="table">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <?php
            $sl=0;
            foreach ($allUser as $user)
            { $sl++;?>
            <td><?php echo $sl; ?></td>
            <td><?php echo $user->id?></td>
            <td><?php echo $user->name?></td>
            <td><?php echo $user->hobbies?></td>
            <td>
                <a href="view.php?id=<?php echo $user->id; ?>" class="btn btn-info">View</a>
                <a href="edit.php?id=<?php echo $user->id; ?>" class="btn btn-success">Edit</a>
                <a href="trash.php?id=<?php echo $user->id; ?>" class="btn btn-primary">Trash</a>
                <a href="delete.php?id=<?php echo $user->id; ?>" class="btn btn-danger">Delete</a>
            </td>

        </tr>
        <?php } ?>
        </tbody>
        </table>
    </div>
<script>
    $('#message').show().delay(2000).fadeout()
</script>
</body>
</html>
