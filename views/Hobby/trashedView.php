<?php
include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Hobby\Hobby;
use App\Bitm\SEIP139740\Utility\Utility;

$user=new Hobby();
$allUser=$user->trashedView();
//Utility::d($allUser);

?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<head>
    <title>Hobby</title>
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="../../Resource/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>


<body>
<div class="container">
    <center><h2>Trashed Hobbies of Users</h2></center>
    <br><br>

    <a href="index.php" class="btn btn-primary">Back To List</a>
    <br><br>
    <form action="recoverMultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-success">Recover Selected</button>
        <button type="button" class="btn btn-danger" id="delete">Delete Selected</button>
        </form>

    <table class="table">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <?php
            $sl=0;
            foreach ($allUser as $user)
            { $sl++;?>
            <td><input type="checkbox" name="mark[]" value="<?php echo $user->id?>"></td>
            <td><?php echo $sl; ?></td>
            <td><?php echo $user->id?></td>
            <td><?php echo $user->name?></td>
            <td><?php echo $user->hobbies?></td>
            <td>
                <a href="recover.php?id=<?php echo $user->id; ?>" class="btn btn-success">Recover</a>
                <a href="delete.php?id=<?php echo $user->id; ?>" class="btn btn-danger">Delete</a>
            </td>

        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $('#delete').on('click',function(){
        document.form[0].action="deleteMultiple.php";
        $('#multiple').submit();
    })
</script>
</body>
</html>
