<?php

include_once('../../vendor/autoload.php');

use App\Bitm\SEIP139740\Hobby\Hobby;
use App\Bitm\SEIP139740\Utility\Utility;

$user=new Hobby();
$user->prepare($_GET);
$singleItem=$user->view();

?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<head>
    <title>Hobby</title>
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="../../Resource/js/bootstrap.min.js"></script>

</head>


<body>
<div class="container">

    <center><h2>View User & Hobby</h2></center>
    <ul class="list-group">
        <li class="list-group-item">ID: <strong><?php echo $singleItem->id;?></strong></li>
        <li class="list-group-item">Name: <strong><?php echo $singleItem->name;?></strong></li>
        <li class="list-group-item">Hobbies: <strong><?php echo $singleItem->hobbies;;?></strong></li>
         </ul>


</div>

</body>

</html>