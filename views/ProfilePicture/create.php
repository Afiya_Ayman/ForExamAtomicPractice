<!Doctype html>
    <html lang="en">
<head>
    <title>Create Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <h2>Create Profile</h2>
        <form role="form" method="post" action="store.php" enctype="multipart/form-data">
            <div class="form-group">
            <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter your Name">
                    </div>
            <div class="form-group">
                <label>Picture</label>
                <input type="file" name="image" class="form-control" placeholder="Upload Picture">
            </div>

            <button type="submit" class="btn btn-primary">Upload</button>
            </form>
        </div>

</body>
</html>