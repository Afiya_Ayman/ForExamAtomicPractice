<?php
include_once('../../vendor/autoload.php');
use App\Bitm\SEIP139740\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139740\Utility\Utility;

$user=new ImageUploader();
$allUser=$user->index();
//Utility::dd($allUser);
?>

<!Doctype html>
<html lang="en">
<head>
    <title>Create Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <h2>List of Users</h2>
    <a href="create.php" class="btn btn-primary" role="button">Insert Again</a>
    <div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>Serial</th>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <?php
            $sL=1;
            foreach($allUser as $item)
            {
            ?>
            <td><?php echo $sL++;?></td>
            <td><?php echo $item->id?></td>
            <td><?php echo $item->name?></td>
            <td><img src="../../Resource/Images/<?php echo $item->images ?>" alt="image" width="100" height="100" class="img-responsive"></td>
            <td>
                <a href="view.php?id=<?php echo $item->id ?>" class="btn btn-info" role="button">View</a>
                <a href="edit.php?id=<?php echo $item->id ?>" class="btn btn-success" role="button">Edit</a>
                <a href="trash.php?id=<?php echo $item->id ?>" class="btn btn-warning" role="button">Trash</a>
                <a href="delete.php?id=<?php echo $item->id ?>" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
        <?php } ?>
        </tbody>
        </table>
        </div>
    </div>
</body>
</html>
