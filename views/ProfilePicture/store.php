<?php

include_once('../../vendor/autoload.php');
use App\Bitm\SEIP139740\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139740\Utility\Utility;

//Utility::d($_POST);
//Utility::d($_FILES);

if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
{
    $image_name=time().$_FILES['image']['name'];
    $temporary_location=$_FILES['image']['tmp_name'];

    move_uploaded_file($temporary_location,'../../Resource/Images/'.$image_name);
    $_POST['images']=$image_name;
}
$user=new ImageUploader();
$user->prepare($_POST);
$user->store();