<?php
include_once('../../vendor/autoload.php');
use App\Bitm\SEIP139740\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139740\Utility\Utility;

$user=new ImageUploader();
$user->prepare($_GET);
$singleItem=$user->view();
//Utility::dd($singleItem);
?>

<!Doctype html>
<html lang="en">
<head>
    <title>Create Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <h2>View Profile</h2>
    <div class="list-group">
        <ul class="list-group">
            <li class="list-group-item list-group-item-success">ID:<strong><?php echo "   ".$singleItem->id ?></strong></li>
            <li class="list-group-item list-group-item-success">Name:<strong><?php echo "   ".$singleItem->name ?></strong></li>
            <li class="list-group-item list-group-item-success">Image:<img src="../../Resource/Images/<?php echo $singleItem->images?>" alt="image" width="200" height="200" class="img-responsive"></strong></li>

        </ul>
        </div>

</div>

</body>
</html>
